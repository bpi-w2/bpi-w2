#!/bin/bash

IMG=2018-09-16-ubuntu-18.04-mate-desktop-demo-aarch64-bpi-w2-sd-emmc.img
DEV=mmcblk1 

dd if=../img/$IMG bs=1MiB | pv | dd of=/dev/$DEV bs=1MiB 
