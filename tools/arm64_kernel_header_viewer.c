#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

/* See Documentation/arm64/booting.txt in the Linux kernel */
struct image_header {
	uint32_t	code0;		/* Executable code */
	uint32_t	code1;		/* Executable code */
	uint64_t	text_offset;	/* Image load offset, LE */
	uint64_t	image_size;	/* Effective Image size, LE */
	uint64_t	res1;		/* reserved */
	uint64_t	res2;		/* reserved */
	uint64_t	res3;		/* reserved */
	uint64_t	res4;		/* reserved */
	uint32_t	magic;		/* Magic number */
	uint32_t	res5;
};

void usage(void)
{
	printf("usage: arm64_kernel_header_viewer <kernel_image>\n");
}

void read_header(char *image_name, struct image_header *ih)
{
	int fd, nr;

	fd = open(image_name, O_RDONLY);
	if (fd == -1) {
		fprintf(stderr, "Error: cannot open kernel image\n");
		exit(1);
	}

	nr = read(fd, (void *)ih, sizeof(struct image_header));
	if (nr != sizeof(struct image_header)) {
		fprintf(stderr, "Error: read %d bytes of %d\n", nr,
			sizeof(struct image_header));
		exit(1);
	}

	if (close(fd)) {
		perror("Error closing kernel image file");
	}
}

void print_header(struct image_header *ih)
{
	printf("code0:       0x%x\n",   ih->code0);
	printf("code1:       0x%x\n",   ih->code1);
	printf("text_offset: 0x%llx\n", ih->text_offset);
	printf("image_size:  0x%llx\n", ih->image_size);
	printf("res1:        0x%llx\n", ih->res1);
	printf("res2:        0x%llx\n", ih->res2);
	printf("res3:        0x%llx\n", ih->res3);
	printf("res4:        0x%llx\n", ih->res4);
	printf("magic:       0x%x\n",   ih->magic);
	printf("res5:        0x%x\n",   ih->res5);
}

int main(int argc, char **argv)
{
	char *image_name;
	struct image_header ih;

	if (argc != 2) {
		usage();
		exit(1);
	}

	image_name = argv[1];

	read_header(image_name, &ih);
	print_header(&ih);

	return 0;
}
