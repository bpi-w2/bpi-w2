#!/bin/bash
#GPL

# exports needed by u-boot build
export ARCH=arm64
export BOARD=BPI-W2-720P
export SOC=rtd1296
export MACH=rtd129x
export CROSS_COMPILE=aarch64-linux-gnu-
export UBOOT_CONFIG=rtd129x_config
export LICHEE_PLATFORM="linux"
export GPU_TYPE="mali820"
export TOPDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export TARGET_PLATFORM=realtek
export TARGET_PRODUCT=bpi-w2

TOOLCHAIN=gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu
COMPILE_TOOL=$TOPDIR/toolchains/$TOOLCHAIN/bin
K_CROSS_COMPILE=$COMPILE_TOOL/aarch64-linux-gnu-
NP=`nproc --all`

MNTDIR=/mnt

# realtek kernel
# KERNEL_TREE=realtek
# KERNEL_CONFIG=rtd129x_bpi_defconfig

# vanilla kernel
KERNEL_TREE=vanilla
KERNEL_CONFIG=bpi-w2_defconfig

SD="$TOPDIR/SD"
KSRC=$TOPDIR/kernel/$KERNEL_TREE/linux
KBLD=$TOPDIR/kernel/$KERNEL_TREE/build
KINS=$TOPDIR/kernel/$KERNEL_TREE/install


USRC=$TOPDIR/u-boot-rt/src

# realtek kernel breaks with O=
if [ "$KERNEL_TREE" == "realtek" ]; then
	KOUT=""
	KBLD=$KSRC
else
	KOUT="O=$KBLD"
fi

##################################################################
# FUNCTIONS
##################################################################

usage() {
	echo "Usage: kbuild.sh [-akud] <device>"
	echo "Usage: kbuild.sh [-b <k|u|d>] [-i <k|u|d|f> <device>]"
	echo "  -a: build all"
	echo "  -k: build and install kernel"
	echo "  -u: build and install U-Boot"
	echo "  -d: build and install dts"
	echo "  -b <k|c|m|u|d|a>:   build   (k)ernel (c)ore) (m)odules (u)boot (d)ts (a)ll"
	echo "  -i <k|c|m|u|d|f|a>: install (k)ernel (c)ore) (m)odules (u)boot (d)ts (f)irmware (a)ll"
	echo "  -c: clean kernel build files"
	echo "  -s: play sound on completion"
}

requires_root() {
	if [ "$(id -u)" -ne 0 ]; then
		echo >&1 "ERROR: This command can only be used by root."
		abort
	fi
}

update_bpi_files() {
	DEV=$1
	FSTYPE=$2
	shift;shift
	FILES=$*
	MNTDIR=/mnt
	TAROPT=""
	echo
	echo "######################"
	echo "update $DEV($FSTYPE) with $FILES"
	echo "######################"
	echo

	if [ "$FSTYPE" == "vfat" ]; then
		TAROPT="--no-same-permissions --no-same-owner"
	fi
	echo "sudo mount -t $FSTYPE $MOPT $DEV $MNTDIR"
	sudo mount -t $FSTYPE $DEV $MNTDIR
	echo
	for IN in ${FILES} ; do
		if [ -f "${IN}" ] ; then
			echo tar -xzf $IN --keep-directory-symlink -C $MNTDIR
			tar -xzf $IN $TAROPT --keep-directory-symlink -C $MNTDIR
		else
			echo "Warning: CAN NOT OPEN BOOTFILE file ${IN}"
		fi
	done
	sudo sync
	ls -al $MNTDIR
	sudo umount $MNTDIR
}

unmount_device() {
	UNMOUNT_DEVICE=$1
	# unmount device if mounted
	for IN in `df | awk '{ print $1 }' | grep "$UNMOUNT_DEVICE"` ; do
		PART=$IN
		echo umount $PART
		sudo umount $PART
	done
}

check_device_exist() {
	DEV=$1
	if [ ! -b $DEV ]; then
		>&2 echo "Error: device $DEV not found"
		abort
	fi
}

check_device_allowed() {
	DEV=$1
	if [ "$DEV" != "/dev/mmcblk1" ]; then
		>&2 echo "ERROR: Device $DEV is not an allowed device."
		>&2 echo " This is a paranoid check to prevent writting where"
		>&2 echo " I should do not :)"
		>&2 echo " you can comment this check in the source code if you"
		>&2 echo " know what you are doing"
		abort
	fi
}

abort() {
	set +e
	trap - DEBUG
	trap - EXIT
	exit 1
}

##################################################################
# SCRIPT SETUP
##################################################################

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

##################################################################
# PARSE ARGUMENTS
##################################################################

DO_BUILD_UBOOT=false
DO_BUILD_KERNEL_CORE=false
DO_BUILD_KERNEL_MODULES=false
DO_BUILD_DTS=false
DO_INSTALL=false
DO_INSTALL_UBOOT=false
DO_INSTALL_KERNEL_CORE=false
DO_INSTALL_KERNEL_MODULES=false
DO_INSTALL_DTS=false
DO_INSTALL_FIRMWARE=false
DO_CLEAN=false
while getopts "akudchi:b:" o; do
	case "${o}" in
		a)
			DO_BUILD_UBOOT=true
			DO_BUILD_KERNEL=true
			DO_BUILD_DTS=true
			DO_INSTALL_UBOOT=true
			DO_INSTALL_KERNEL=true
			DO_INSTALL_DTS=true
			DO_INSTALL_FIRMWARE=true
			;;
		u)
			DO_BUILD_UBOOT=true
			DO_INSTALL_UBOOT=true
			;;
		k)
			DO_BUILD_KERNEL=true
			DO_INSTALL_KERNEL=true
			;;
		d)
			DO_BUILD_DTS=true
			DO_INSTALL_DTS=true
			;;
		b)
			BUILD_TYPE=$OPTARG
			for (( i=0; i<${#BUILD_TYPE}; i++)); do
				OPT=${BUILD_TYPE:$i:1}
				if [ "$OPT" == "u" ]; then
					DO_BUILD_UBOOT=true
				elif [ "$OPT" == "k" ]; then
					DO_BUILD_KERNEL_CORE=true
					DO_BUILD_KERNEL_MODULES=true
				elif [ "$OPT" == "c" ]; then
					DO_BUILD_KERNEL_CORE=true
				elif [ "$OPT" == "m" ]; then
					DO_BUILD_KERNEL_MODULES=true
				elif [ "$OPT" == "d" ]; then
					DO_BUILD_DTS=true
				elif [ "$OPT" == "a" ]; then
					DO_BUILD_UBOOT=true
					DO_BUILD_KERNEL=true
					DO_BUILD_DTS=true
				else
					1>&2 echo "Error: option $OPT not recognized"
					usage
					abort
				fi
			done
			;;
		i)
			INSTALL_TYPE=$OPTARG
			for (( i=0; i<${#INSTALL_TYPE}; i++)); do
				OPT=${INSTALL_TYPE:$i:1}
				if [ "$OPT" == "u" ]; then
					DO_INSTALL_UBOOT=true
				elif [ "$OPT" == "k" ]; then
					DO_INSTALL_KERNEL_CORE=true
					DO_INSTALL_KERNEL_MODULES=true
				elif [ "$OPT" == "c" ]; then
					DO_INSTALL_KERNEL_CORE=true
				elif [ "$OPT" == "m" ]; then
					DO_INSTALL_KERNEL_MODULES=true
				elif [ "$OPT" == "d" ]; then
					DO_INSTALL_DTS=true
				elif [ "$OPT" == "f" ]; then
					DO_INSTALL_FIRMWARE=true
				elif [ "$OPT" == "a" ]; then
					DO_INSTALL_UBOOT=true
					DO_INSTALL_KERNEL=true
					DO_INSTALL_DTS=true
					DO_INSTALL_FIRMWARE=true
				else
					1>&2 echo "Error: option $OPT not recognized"
					usage
					abort
				fi
			done
			;;
		c)
			DO_CLEAN=true
			;;
		h)
			usage
			exit 0
			;;
		\?)
			echo "option not recognized"
			usage
			abort
			;;
	esac
done

shift $( expr $OPTIND - 1 )

if [ "$DO_INSTALL_UBOOT"          == "true" ] ||
   [ "$DO_INSTALL_KERNEL_CORE"    == "true" ] ||
   [ "$DO_INSTALL_KERNEL_MODULES" == "true" ] ||
   [ "$DO_INSTALL_DTS"            == "true" ] ||
   [ "$DO_INSTALL_FIRMWARE"       == "true" ]
then
	DO_INSTALL=true
fi

if [ "$DO_INSTALL" == "true" ]; then
	if [ $# -ne 1 ]; then
		usage
		abort
	fi
	DEVICE=$1
	check_device_allowed $DEVICE
else
	if [ $# -ne 0 ]; then
		usage
		abort
	fi
fi

##################################################################
# BUILD U-Boot
##################################################################

UBOOTBIN=u-boot.bin
UBOOTIMG=u-boot.img

if [ "$DO_BUILD_UBOOT" == "true" ]; then
	echo
	echo "###################"
	echo "# Building U-Boot #"
	echo "###################"
	echo

	cd $USRC
	rm -f $UBOOTBIN $UBOOTIMG

	PATH=$COMPILE_TOOL:$PATH make clean
	PATH=$COMPILE_TOOL:$PATH make rtd1296_sd_bananapi_defconfig
	PATH=$COMPILE_TOOL:$PATH make CONFIG_CHIP_TYPE=0002 BUILD_BOOTCODE_ONLY=true

	dd if=/dev/zero of=$UBOOTIMG bs=1k count=1022 status=none
	dd if=$UBOOTBIN of=$UBOOTIMG bs=1k seek=38 status=none

	cd $TOPDIR
fi

##################################################################
# BUILD Kernel
##################################################################

if [ "$DO_CLEAN" == "true" ]; then
	echo
	echo "###################"
	echo "# Cleaning Kernel #"
	echo "###################"
	echo

	cd $KSRC
	make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE clean
	cd $TOPDIR
fi

if [ "$DO_BUILD_KERNEL_CORE" == "true" ]; then
	echo
	echo "########################"
	echo "# Building Kernel Core #"
	echo "########################"
	echo

	cd $KSRC
	#make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE $KERNEL_CONFIG
	make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE -j$NP Image

	# TODO build bpi out-of-tree drivers
	#mkdir $(OUTPUT_DIR)/lib/modules/4.9.119-BPI-W2-Kernel/kernel/extra
	#$(Q)$(MAKE) -C phoenix/system/src/drivers ARCH=arm64 CROSS_COMPILE=${K_CROSS_COMPILE} TARGET_KDIR=$(TARGET_KDIR) -j$J INSTALL_MOD_PATH=output
	#$(Q)$(MAKE) -C phoenix/system/src/drivers ARCH=arm64 CROSS_COMPILE=${K_CROSS_COMPILE} TARGET_KDIR=$(TARGET_KDIR) -j$J INSTALL_MOD_PATH=output install
	#$(Q)$(MAKE) -C linux-rt ARCH=arm64 CROSS_COMPILE=${K_CROSS_COMPILE} -j$J INSTALL_MOD_PATH=output _depmod
	cd $TOPDIR
fi

if [ "$DO_BUILD_KERNEL_MODULES" == "true" ]; then
	echo
	echo "###########################"
	echo "# Building Kernel Modules #"
	echo "###########################"
	echo

	rm -rf $KINS

	cd $KSRC

	make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE -j$NP modules
	make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE -j$NP INSTALL_MOD_PATH=$KINS modules_install

	cd $TOPDIR
fi

if [ "$DO_BUILD_DTS" == "true" ]; then
	echo
	echo "################"
	echo "# Building DTS #"
	echo "################"
	echo

	cd $KSRC
	make ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE -j$NP INSTALL_MOD_PATH=$KINS dtbs
	cd $TOPDIR
fi

##################################################################
# CREATE IMAGE FROM SCRATCH
##################################################################

# TODO http://forum.banana-pi.org/t/how-to-build-an-ubuntu-debian-sd-image-from-scratch/6805


##################################################################
# INSTALL COMPONENTS
##################################################################

# BPI SD partition layout: SIZE=7456 (MB) for eMMC/SD
#    0:100MB: RAW DATA: unallocated area, no label,
#      (0-100MB)        a. bootloader like u-boot
#                       b. boot logo
#                       c. env
#                       d. android kernel
#    1:256MB:      FAT: label BPI-BOOT,
#      (100MB-356MB)    a. uEnv.txt / boot.scr
#                       b. kernel & initrd
#                       c. script.bin / dtb
#    2: ~END:     EXT4: label BPI-ROOT,
#      (356MB~END)      for rootfs like ubuntu / debian ...


if [ "$DO_INSTALL" == "true" ]; then
	check_device_exist $DEVICE
	unmount_device $DEVICE
fi

if [ "$DO_INSTALL_UBOOT" == "true" ]; then
	echo
	echo "#####################"
	echo "# Installing U-Boot #"
	echo "#####################"
	echo

	# skip first 2KiB ouf device because they contain the MBR and ?
	# the U-Boot binary ends up written to the first 40KiB of the SD
	dd if=$USRC/$UBOOTIMG of=$DEVICE bs=1k seek=2
fi

if [ "$DO_INSTALL_KERNEL_CORE" == "true" ]; then
	echo
	echo "##########################"
	echo "# Installing Kernel Core #"
	echo "##########################"
	echo

	sudo mount -t vfat "${DEVICE}p1" $MNTDIR
	sudo cp $KBLD/arch/arm64/boot/Image $MNTDIR/bananapi/$TARGET_PRODUCT/linux/uImage
	sudo umount $MNTDIR
fi

if [ "$DO_INSTALL_KERNEL_MODULES" == "true" ]; then
	echo
	echo "#############################"
	echo "# Installing Kernel Modules #"
	echo "#############################"
	echo

	KVER=$(cd $KSRC; make -s ARCH=arm64 $KOUT CROSS_COMPILE=$K_CROSS_COMPILE kernelrelease)

	sudo mount -t ext4 "${DEVICE}p2" $MNTDIR
	# paranoid check
	if [ "$MNTDIR" != "" ] && [ "$MNTDIR" != "/" ]; then
		sudo rm -rf $MNTDIR/lib/modules/*
	else
		>&2 echo "Invalid mount directory \"$MNTDIR\""
		abort
	fi
	sudo cp -r $KINS/lib/modules/$KVER $MNTDIR/lib/modules/
	sudo umount $MNTDIR
fi

if [ "$DO_INSTALL_DTS" == "true" ]; then
	echo
	echo "##################"
	echo "# Installing DTS #"
	echo "##################"
	echo

	sudo mount -t vfat "${DEVICE}p1" $MNTDIR
	if [ "$KERNEL_TREE" == "realtek" ]; then
		sudo cp $KBLD/arch/arm64/boot/dts/realtek/rtd129x/rtd-1296-bananapi-w2-2GB-HDMI.dtb $MNTDIR/bananapi/$TARGET_PRODUCT/linux/bpi-w2.dtb
	else
		sudo cp $KBLD/arch/arm64/boot/dts/realtek/rtd1296-bananapi-bpi-w2.dtb $MNTDIR/bananapi/$TARGET_PRODUCT/linux/bpi-w2.dtb
	fi
	sudo umount $MNTDIR
fi

if [ "$DO_INSTALL_FIRMWARE" == "true" ]; then
	echo
	echo "#######################"
	echo "# Installing Firmware #"
	echo "#######################"
	echo

	sudo mount -t vfat "${DEVICE}p1" $MNTDIR
	#TODO can I remove bpi-boot-files/dtb folder?
	sudo cp -r $TOPDIR/bpi-boot-files/* $MNTDIR/bananapi/$TARGET_PRODUCT/linux/
	sudo umount $MNTDIR

	# TODO copy the rest of the files
	#cp $TOPDIR/bpi-root-files/ .
	# TODO: restore original files
	#BPIFILES="bpi-tools.tgz
	#bpi-w2-tools.tgz
	#bpi-service.tgz
	#brcm.tgz"
fi

if [ "$DO_INSTALL" == "true" ]; then
	sudo sync
fi

set +e
trap - DEBUG
trap - EXIT
