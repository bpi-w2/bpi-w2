#!/bin/bash

TOPDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
KERNEL_TREE=vanilla
KERNEL_CONFIG=bpi-w2_defconfig

KSRC=$TOPDIR/kernel/$KERNEL_TREE/linux
KBLD=$TOPDIR/kernel/$KERNEL_TREE/build
KINS=$TOPDIR/kernel/$KERNEL_TREE/install

qemu-system-aarch64 \
	-machine virt -cpu cortex-a53 \
	-kernel $KBLD/arch/arm64/boot/Image \
	-initrd $TOPDIR/rootfs/busybox/ramdisk.img \
	-dtb $KBLD/arch/arm64/boot/dts/realtek/bpi-w2.dtb \
	-append "console=ttyAMA0" \
	-nographic

