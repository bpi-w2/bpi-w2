#!/bin/bash

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

NP=`nproc --all`

TOPDIR=/home/aleix/projects/kernel/bpi-w2/my_sdk
TOOLCHAIN=gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu
COMPILE_TOOL=$TOPDIR/toolchains/$TOOLCHAIN/bin
K_CROSS_COMPILE=$COMPILE_TOOL/aarch64-linux-gnu-

#rm -rf build
rm -rf install
mkdir -p build install

cd src
#make O=../build ARCH=arm64 defconfig
make O=../build ARCH=arm64 clean
make O=../build ARCH=arm64 -j$NP CROSS_COMPILE=$K_CROSS_COMPILE
make O=../build ARCH=arm64 -j$NP CROSS_COMPILE=$K_CROSS_COMPILE CONFIG_PREFIX=../install install
cd ..

cp -a install/bin/busybox rootfs/bin/busybox
chmod u+s rootfs/bin/busybox

set +e
trap - DEBUG
trap - EXIT
