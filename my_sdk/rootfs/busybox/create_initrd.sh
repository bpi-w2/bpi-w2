#!/bin/bash

MNTDIR=/mnt
RAMDISK=./ramdisk.img
ROOTFS=./rootfs

rm -f $RAMDISK

# Ramdisk Constants
RDSIZE=$((4*1024)) # kibibytes
RDBLKS=1024

sudo modprobe loop
 
# Create an empty ramdisk image
dd if=/dev/zero of=$RAMDISK bs=$RDBLKS count=$RDSIZE
 
# Make it an ext2 mountable file system
/sbin/mke2fs -F -m 0 -b $RDBLKS $RAMDISK $RDSIZE
 
# Mount it so that we can populate
sudo mount $RAMDISK $MNTDIR  -t ext2 -o loop=/dev/loop0
sudo cp -a $ROOTFS/* $MNTDIR/
 
## Grab busybox and create the symbolic links
#pushd /mnt/initrd/bin
#cp /usr/local/src/busybox-1.1.1/busybox .
#ln -s busybox ash
#ln -s busybox mount
#ln -s busybox echo
#ln -s busybox ls
#ln -s busybox cat
#ln -s busybox ps
#ln -s busybox dmesg
#ln -s busybox sysctl
#popd
# 
## Grab the necessary dev files
#cp -a /dev/console /mnt/initrd/dev
#cp -a /dev/ramdisk /mnt/initrd/dev
#cp -a /dev/ram0 /mnt/initrd/dev
#cp -a /dev/null /mnt/initrd/dev
#cp -a /dev/tty1 /mnt/initrd/dev
#cp -a /dev/tty2 /mnt/initrd/dev
# 
## Equate sbin with bin
#pushd /mnt/initrd
#ln -s bin sbin
#popd
 
# Create the init file
#cat >> /mnt/initrd/linuxrc << EOF
##!/bin/ash
#echo
#echo "Simple initrd is active"
#echo
#mount -t proc /proc /proc
#mount -t sysfs none /sys
#/bin/ash --login
#EOF
 
#chmod +x /mnt/initrd/linuxrc
 
# Finish up...
sudo umount $MNTDIR
#gzip -9 $RAMDISK
