#!/bin/bash

cd initramfs
find . | cpio -H newc -o > ../initramfs.cpio
cd ..
cat initramfs.cpio | gzip > initramfs.igz
rm initramfs.cpio

#install to bananapi/bpi-w2/linux/uInitrd
